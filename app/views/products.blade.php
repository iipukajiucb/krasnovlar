<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div>
    <form id='form1' action="<?php echo route('productsGet')?>" method="GET">
        <select name="limit">
            <option value="10"  @if ($limit==10) selected @endif >10</option>
            <option value="20"  @if ($limit==20) selected @endif >20</option>
            <option value="50"  @if ($limit==50) selected @endif >50</option>
        <select>
        <select name="filter">
            <option value="nameasc"  @if ($filter=='nameasc') selected @endif >а-я</option>
            <option value="namedsc"  @if ($filter=='namedsc') selected @endif >я-а</option>
            <option value="pricedsc"  @if ($filter=='pricedsc') selected @endif >цена от больш к меньш</option>
            <option value="priceasc"  @if ($filter=='priceasc') selected @endif >цена от меньш к больш</option>
            <option value="yeardsc"  @if ($filter=='yeardsc') selected @endif >Год от больш к меньш</option>
            <option value="yearasc"  @if ($filter=='yearasc') selected @endif >Год от меньш к больш</option>			
        </select>
        <input type='button' value="Отправить" onclick = 'myFunction();'>
    </form>
    <script>
        function myFunction() 
        {
            var limit = form1.elements["limit"].selectedIndex; 
            var filter = form1.elements["filter"].selectedIndex;  
                if((limit==1) && (filter==0))
            {
                document.location.replace("/products");
            }
            else
            {
                form1.submit();
            }
        }
    </script>	
    @if ($limit==20 && $filter=='nameasc') 
    {{$data->links()}}
    @else  {{$data->appends(['limit' => $limit, 'filter' => $filter])->links()}}
    @endif	   
        <div class="table_block">
            <table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>price</th>
                        <th>description</th>
                        <th>year</th>
                        <th>created</th>
                        <th>updated</th>
                    </tr>
                </thead>
                <tbody>					
                    @foreach ($data as $product) 
                        <tr>				
                            @foreach ($product->toArray() as $item)
                                <td>{{$item}}</td>
                            @endforeach
                        </tr>
                    @endforeach					
                </tbody>
            </table>
        </div>
    @if ($limit==20 && $filter=='nameasc') 
    {{$data->links()}}
    @else  {{$data->appends(['limit' => $limit, 'filter' => $filter])->links()}}
    @endif
    </div>
</body>
</html>