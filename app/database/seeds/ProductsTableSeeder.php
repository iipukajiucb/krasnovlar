<?php

class ProductsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
         foreach (range(1,105) as $index) {
             Products::create([
                //numberBetween($min = 100, $max = 900)
                 'name'=>$faker->name(),
                 'price'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
                 'description'=>$faker->sentence($nbWords = 6, $variableNbWords = true),
                 'year'=>$faker->numberBetween($min = 1950, $max = 2020),
                 
             ]);
         }
    }

}
