<?php

Route::get('/', function()
{
    return View::make('hello');
});
Route::get('/products', ['as' => 'productsGet', 'uses' => 'ProductsController@index']);