<?php

class ProductsController extends \BaseController 
{
    public function index()
    {
        $page = Input::get('page', 1);
        $limit = Input::get('limit', 20);
        $filter = Input::get('filter', 'nameasc');
        $field = substr(Input::get('filter', 'nameasc'), 0, -3);
        $direction = substr(Input::get('filter', 'nameasc'), -3);
        $data = Products::orderBy($field,$direction)->paginate($limit);
        return View::make('products', ['data' => $data, 'limit' => $limit, 'filter' => $filter]);
    }
}
